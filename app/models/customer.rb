class Customer < ApplicationRecord
  has_many :articles
  has_many :employees, as: :employable
end
