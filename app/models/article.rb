class Article < ApplicationRecord

  belongs_to :customer

  enum type_enum: { 'Wheel': 0, 'Vehicle': 1, 'Truck': 2 }
  enum second_enum: { 'House': 0, 'Garden': 1 }

end
