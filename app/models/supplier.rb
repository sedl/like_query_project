class Supplier < ApplicationRecord
  has_many :employees, as: :employable
end
