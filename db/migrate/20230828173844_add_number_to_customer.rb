class AddNumberToCustomer < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :number, :string
  end
end
