class AddSecondEnumToArticle < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :second_enum, :integer
  end
end
