class AddEnumToArticle < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :type, :integer
  end
end
