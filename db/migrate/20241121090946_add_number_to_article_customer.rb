class AddNumberToArticleCustomer < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :price, :float
    add_column :customers, :employees_count, :integer
  end
end
