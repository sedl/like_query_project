class AddArticleStockCount < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :stock_count, :integer
    remove_column :customers, :employees_count
  end
end
