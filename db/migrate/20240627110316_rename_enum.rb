class RenameEnum < ActiveRecord::Migration[7.0]
  def change
    rename_column :articles, :type, :type_enum
  end
end
