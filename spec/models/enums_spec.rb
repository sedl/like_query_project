require 'rails_helper'

RSpec.describe Article, type: :model do

  let(:customer) { Customer.create(name: 'Ambühl') }

  before(:each) do
    Customer.destroy_all
    Article.destroy_all
  end

  context 'enums' do

    let(:article) { FactoryBot.create(:article, name: 'Super-Article', type_enum: 'Truck', second_enum: 'Garden') }

    it 'control' do
      article
      arts = Article.where(type_enum: 'Truck')
      expect(arts.count).to eq(1)
    end

    it 'find Truck' do
      article
      arts = Article.like('Truck', [:type_enum, :second_enum])
      expect(arts.count).to eq(1)
    end

    it 'find Garden' do
      article
      arts = Article.like('Garden', [:type_enum, :second_enum])
      expect(arts.count).to eq(1)
    end

    it 'find :name' do
      article
      arts = Article.like('super', [:name, :type_enum, :second_enum])
      expect(arts.count).to eq(1)
    end

  end

end