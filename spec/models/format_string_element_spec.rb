require 'rails_helper'

RSpec.describe 'Format String Element', type: :model do

  let(:dummy_class) { Class.new { extend LikeQuery::ModelExtensions } }

  it 'String as Full-Text-Search' do
    r = dummy_class.send(:format_string_element, 'hi', true)
    expect(r[:string]).to eq('hi')
  end

  it 'Integer' do
    r = dummy_class.send(:format_string_element, '234', true)
    expect(r[:float_from]).to eq('234')
    expect(r[:float_until]).to eq('234')
  end

  it 'Float' do
    r = dummy_class.send(:format_string_element, '234.234', true)
    expect(r[:float_from]).to eq('234.234')
    expect(r[:float_until]).to eq('234.234')
  end

  it 'Range' do
    r = dummy_class.send(:format_string_element, '234.234..23.234', true)
    expect(r[:float_from]).to eq(234.234)
    expect(r[:float_until]).to eq(23.234)
    expect(r[:column]).to eq(nil)
  end

  it 'when case-sensitive then match' do
    r = dummy_class.send(:format_string_element, 'name:reto', true)
    expect(r[:column]).to eq('name')
    expect(r[:string]).to eq('reto')
  end

  it 'match associated column' do
    r = dummy_class.send(:format_string_element, 'customer.name:reto', true)
    expect(r[:column]).to eq('customer.name')
    expect(r[:string]).to eq('reto')
  end

end