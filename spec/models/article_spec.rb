require 'rails_helper'

RSpec.describe Article, type: :model do

  let(:customer) { Customer.create(name: 'Ambühl') }

  before(:each) do
    Customer.destroy_all
    Article.destroy_all
  end

  it 'create one' do
    art = FactoryBot.create(:article, name: 'first', number: '01', customer: customer)
    expect(art.id.present?).to be_truthy
  end

  it '.find' do
    art = Article.create(name: 'first', number: '01', customer: customer)
    expect(Article.like('first', :name, debug: true)).to include(art)
  end

  it 'find2 by ""' do
    art = Article.create(name: 'first', number: '01', customer: customer)
    expect(Article.like('', :name)).to include(art)
  end

  it 'find by nil' do
    art = Article.create(name: 'first', number: '01', customer: customer)
    expect(Article.like(nil, :name)).to include(art)
  end

  it 'find by false' do
    art = Article.create(name: 'first', number: '01', customer: customer)
    expect(Article.like(false, :name)).to include(art)
  end

  it 'generate_hash' do
    art = Article.create(name: 'first', number: '01', customer: customer)

    r = Article.like('first', :name).generate_hash(:number)
    expect(r[:data].first[:values]).to eq(["01"])
    expect(r[:data].first[:id]).to eq(art.id)
    expect(r[:data].first[:model]).to eq("Article")
    expect(r[:data].first[:attributes]).to eq({ :number => '01' })

    r1 = Article.like('first', :name).generate_hash
    expect(r1[:data].first[:values]).to eq(["first"])

    r2 = Article.like('first', :name).generate_hash(:number, limit: 0, image: 'my-image')
    expect(r2[:data]).to eq([])
  end

  it 'generate_json' do
    Article.create(name: 'first', number: '01', customer: customer)

    h = Article.like('first', :name).generate_hash(:number)
    expect(h[:data].first[:values]).to eq(["01"])

    js = Article.like('first', :name).generate_json(:number)
    expect(JSON.parse(js)['data'].first['values']).to eq(["01"])

    c = LikeQuery::Collect.new
    c.collect { Article.like('first', :name) }
    js = c.generate_json
    expect(JSON.parse(js)['data'].first['values']).to eq(["first"])
  end

  it 'generate_hash: customers articles with image' do
    customer = Customer.create(name: 'Ambühl', image: 'src:customer-image')
    Article.create(name: 'first', number: '01', image: 'src:article-image', customer: customer)
    Article.create(name: 'second', number: '01', image: 'src:article-image', customer: customer)

    r = Customer.like('ambühl', :name).generate_hash([:name, articles: { values: [:number] }], image: :image)
    expect(r[:data].first[:image]).to eq("src:customer-image")
    expect(r[:image]).to eq(true)
  end

  it 'null values' do
    customer = Customer.create(name: 'Ambühl', number: nil)
    Article.create(name: 'first', number: nil, customer: customer)

    r = Customer.like('ambühl', :name).generate_hash([:name, :number, articles: [:name, :number]])
    expect(r[:data].first[:values].second).to eq('')
    # expect(r[:data].first[:associations][:articles].first[:values].second).to eq('')

  end

  it 'collect with image' do
    Customer.create(name: 'Ambühl', image: 'src:customer-image')
    c = LikeQuery::Collect.new(4)
    c.collect(image: :image) { Customer.like('ambü', :name) }
    expect(c.generate_hash[:image]).to eq(true)
    expect(c.generate_hash[:data].first[:image]).to eq('src:customer-image')
  end

  it 'generate_hash: customers articles without image' do
    customer = Customer.create(name: 'Ambühl', image: 'src:customer-image')
    Article.create(name: 'first', number: '01', image: 'src:article-image', customer: customer)
    Article.create(name: 'second', number: '01', image: 'src:article-image', customer: customer)

    r = Customer.like('ambühl', :name).generate_hash([:name, articles: { values: [:number] }])
    expect(r[:image]).to eq(false)
    # expect(r[:sub_records_image]).to eq(false)
  end

  it 'should work behind standard queries, independent from #like' do

    Article.create(number: '01', customer: customer)
    Article.instance_variable_set(:@like_query_schema, nil)
    a = Article.all.generate_hash(:number)

    expect(a[:data].first[:values]).to eq(['01'])
    expect(a[:length]).to eq(1)
  end

  it 'like must not work on ApplicationRecord class' do
    c = LikeQuery::Collect.new
    expect { c.get }.to raise_error(NoMethodError)

    expect { ApplicationRecord.like('first') }.to raise_error(RuntimeError, 'like can only be called from a model')
  end

  it 'collect' do

    cust = Customer.create(name: 'Müller')
    2.times { Article.create(name: 'first', number: '01', customer: cust) }

    cust2 = Customer.create(name: 'Ambühl')
    3.times { Article.create(name: 'second', number: '01', customer: cust2) }
    1.times { Article.create(name: 'third', number: '01', customer: cust2) }

    c = LikeQuery::Collect.new(4)

    expect(c.collect('customer.name') { Article.like('müller', [:name, customer: :name]) }).to eq(true)
    expect(c.collect { Article.like('second', [:name, :number, customer: :name]) }).to eq(true)
    # now we are full and expect collect-method to do nothing
    expect(c.collect([:id, :number]) { Article.like('third', :name) }).to eq(false)

    expect(c.generate_hash[:data].length).to eq(4)
    expect(c.generate_hash[:length]).to eq(4) # length including sub-records
    expect(c.generate_hash[:columns_count]).to eq(2)
    expect(c.generate_hash[:overflow]).to eq(true)
    # expect(c.generate_hash[:sub_records_columns_count]).to eq(1)
    expect(c.generate_hash[:time]).to be > 0.00001
    puts "Collect time: #{c.generate_hash[:time]}"
  end

  it 'declare and remind schema without records' do
    Article.destroy_all
    Article.instance_variable_set(:@like_query_schema, nil)
    c = LikeQuery::Collect.new

    c.collect([:name]) { Article.all }
    expect(c.instance_variable_get(:@schemes)).to eq({ 'Article' => { :values => [:name] } })

    # empty pattern is ignored
    c.collect { Article.all }
    expect(c.instance_variable_get(:@schemes)).to eq({ 'Article' => { :values => [:name] } })

    # overwrite
    c.collect([:number]) { Article.all }
    expect(c.instance_variable_get(:@schemes)).to eq({ 'Article' => { :values => [:number] } })

    # get from like
    c.collect() { Article.like('xxx', :id) }
    expect(c.instance_variable_get(:@schemes)).to eq({ 'Article' => { :values => [:id] } })
  end

  it 'record_to_hash' do
    a = Article.create(number: 'abc', image: 'src:img')
    c = LikeQuery::Collect.new

    s = c.send(:schema_to_hash, [:number])
    r = c.send(:record_to_hash, a, s, :image)
    expect(r[:values]).to eq(['abc'])
    expect(r[:id]).to eq(a.id)
    expect(r[:model]).to eq('Article')
    expect(r[:image]).to eq('src:img')

    # without image
    s = c.send(:schema_to_hash, [:number])
    r2 = c.send(:record_to_hash, a, s, nil)
    expect(r2.keys).not_to include(:image)
  end

  it 'get_column_value()' do
    c = Customer.create(number: 'cust', name: 'cust-nam')
    a = Article.create(number: 'art', customer: c)
    c = LikeQuery::Collect.new

    expect(
      c.send(:get_column_value, a, :number)
    ).to eq('art')

    expect(
      c.send(:get_column_value, a, { customer: :number })
    ).to eq('cust')

    expect(
      c.send(:get_column_value, a, { customer: [:number, :name] })
    ).to eq('cust, cust-nam')

    expect {
      c.send(:get_column_value, a, { customer: [:number, { a: :b }] })
    }.to raise_error(RuntimeError, "Too deeply nested objects: [:number, {:a=>:b}]")

    expect(
      c.send(:get_column_value, a, 'customer.number')
    ).to eq('cust')
  end

  it 'schema_to_hash()' do
    c = LikeQuery::Collect.new
    expect(c.send(:schema_to_hash, :name)).to eq({ values: [:name] })
    expect(c.send(:schema_to_hash, [:name, image: 'image-value'])).to eq({ values: [:name], image: 'image-value' })
    expect(c.send(:schema_to_hash, { any: :hash })).to eq({ any: :hash })
    # errors
    expect {
      c.send(:schema_to_hash, Article.new)
    }.to raise_exception(RuntimeError, /invalid schema format/)
    expect {
      c.send(:schema_to_hash, [[1]])
    }.to raise_exception(RuntimeError, /invalid schema format[\s\S]+in schema =>/)
  end

  it 'chainable with standard query methods' do
    c = Customer.create(name: 'cust')
    e = Employee.create(name: 'hans', employable: c)
    e = Employee.create(name: 'ulrich', employable: c)
    s = Supplier.create(name: 'suppl')
    e2 = Employee.create(name: 'ulrich', employable: s)

    # where behind like
    r = Employee.like('ulrich', :name).where(employable_type: 'Customer').generate_hash
    expect(r[:length]).to eq(1)
    expect(r[:data].first[:id]).to eq(e.id)

    # like behind where
    r2 = Employee.where(employable_type: 'Customer').like('ulrich', :name).generate_hash
    expect(r2[:length]).to eq(1)
    expect(r2[:data].first[:id]).to eq(e.id)
  end

  it 'raise error unless parent is valid' do
    c = Customer.create(name: 'cust')
    a = Article.create(name: 'screw', customer: c)

    # where behind like
    c = LikeQuery::Collect.new
    expect {
      c.collect(parent: :employable) { Article.like('screw', :name) }
    }.to raise_exception(RuntimeError, 'parent «employable» is not a valid association')
  end

  it 'set schema on not-polymorphic parent' do
    cust = Customer.create(name: 'cust')
    art = Article.create(name: 'screw', customer: cust)

    # where behind like
    c = LikeQuery::Collect.new
    c.set_schema(Customer, :name)
    c.collect(parent: :customer) { Article.like('screw', :name) }
    r = c.generate_hash

    expect(r[:length]).to eq(2)
    expect(r[:data].first[:id]).to eq(cust.id)
    expect(r[:data].first[:values]).to eq(['cust'])
    expect(r[:data].first[:children].first[:values]).to eq(['screw'])
    expect(r[:data].first[:children].first[:model]).to eq('Customer.Article')
    expect(r[:data].first[:children].first[:parent_id]).to eq(cust.id)
    expect(r[:data].first[:children].first[:id]).to eq(art.id)
  end

  it 'set_schema on polymorphic parent' do
    cust = Customer.create(name: 'cust', number: '542')
    e = Employee.create(name: 'ulrich', employable: cust)

    # where behind like
    c = LikeQuery::Collect.new
    c.set_schema(Customer, :number)
    c.collect(parent: :employable) { Employee.like('ulrich', :name) }
    r = c.generate_hash
    expect(r[:length]).to eq(2)
    expect(r[:data].first[:id]).to eq(cust.id)
    expect(r[:data].first[:children].first[:values]).to eq(['ulrich'])

    # check if schema is applied
    expect(r[:data].first[:values]).to eq(['542'])
  end

  it 'performance check for parent and child polymorphic' do
    c = Customer.create(name: 'cust', number: '542')
    5.times { Employee.create(name: 'ulrich', employable: c) }

    c = LikeQuery::Collect.new
    c.set_schema(Customer, :number)
    c.collect(parent: :employable) { Employee.like('ulrich', :name) }

    # this should add the #includes(:employable) inside collect so that a database request to customers is done only once
    # => you can see the result in the RSpec log

    r = c.generate_hash
    expect(r[:length]).to eq(6)
  end

  it 'overflow, limit' do
    cust = Customer.create(name: 'name')
    10.times { Article.create(name: 'art', customer: cust) }
    # at config/application has to be:
    # config.x.like_query.limit = 8
    c = LikeQuery::Collect.new
    c.collect { Article.like('art', :name) }
    expect(c.generate_hash[:length]).to eq(8)

    # override
    c2 = LikeQuery::Collect.new(10)
    c2.collect { Article.like('art', :name) }
    expect(c2.generate_hash[:length]).to eq(10)
  end

  context 'url generation' do

    it 'generate_hash: customers url' do
      customer = Customer.create(name: 'Ambühl')
      proc1 = proc { |cust| Rails.application.routes.url_helpers.customer_path(cust) }
      r = Customer.like('ambühl', :name).generate_hash(:name, url: proc1)
      expect(r[:data].first[:url]).to eq("/customer/#{customer.id}")
      expect(r[:length]).to eq(1)
    end

    it 'collect: customers url by collect' do
      customer = Customer.create(name: 'Ambühl')
      c = LikeQuery::Collect.new
      proc1 = proc { |cust| Rails.application.routes.url_helpers.customer_path(cust) }
      c.collect(url: proc1) { Customer.like('ambühl', :name) }
      r = c.generate_hash
      expect(r[:data].first[:url]).to eq("/customer/#{customer.id}")
      expect(r[:length]).to eq(1)
    end

    it 'generate_hash: articles customer url by collect' do
      customer = Customer.create(name: 'Ambühl')
      art = Article.create(number: 'art-no', customer: customer)
      c = LikeQuery::Collect.new
      proc1 = proc { |cust| Rails.application.routes.url_helpers.customer_path(cust) }
      c.set_schema(Customer, url: proc1)
      c.collect(parent: :customer) { Article.like('art', :number) }
      r = c.generate_hash
      expect(r[:data].first[:url]).to eq("/customer/#{customer.id}")
      expect(r[:length]).to eq(2)
    end

    it 'collect: articles customer url by collect (remind by first search)' do
      customer = Customer.create(name: 'Ambühl')
      art = Article.create(number: 'art-no', customer: customer)
      c = LikeQuery::Collect.new
      proc1 = proc { |cust| Rails.application.routes.url_helpers.customer_path(cust) }

      c.collect(url: proc1) { Customer.like('xyxyxyyxy', :number) }
      # should remember url although nothing found

      proc2 = proc { |article| Rails.application.routes.url_helpers.customer_article_path(article.customer, article) }
      c.collect(parent: :customer, url: proc2) { Article.like('art', :number) }
      r = c.generate_hash
      expect(r[:data].first[:url]).to eq("/customer/#{customer.id}")
      expect(r[:data].first[:children].first[:url]).to eq("/customer/#{customer.id}/article/#{art.id}")
      expect(r[:length]).to eq(2)
    end

  end

  context 'action modifiers' do
    it 'set no_action on parent' do
      customer = Customer.create(name: 'Ambühl')
      art = Article.create(number: 'art-no', customer: customer)
      c = LikeQuery::Collect.new
      c.set_schema(Customer, no_action: true)
      c.collect(parent: :customer) { Article.like('art', :number) }
      r = c.generate_hash
      expect(r[:data].first[:no_action]).to eq(true)
      expect(r[:data].first[:id]).to eq(customer.id)
      expect(r[:length]).to eq(2)
    end
  end

end