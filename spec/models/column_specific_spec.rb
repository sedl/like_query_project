require 'rails_helper'

RSpec.describe Article, type: :model do

  before(:each) do
    Customer.destroy_all
    Article.destroy_all
  end

  it 'find within column' do
    art = FactoryBot.create(
      :article,
      name: 'hugo',
      number: 'alert'
    )
    expect(
      Article.like(
        'name:hugo',
        [:name, :number]
      )
    ).to include(art)
  end

  it 'find not within wrong column' do
    art = FactoryBot.create(
      :article,
      name: 'hugo',
      number: 'reto'
    )
    expect(
      Article.like(
        'name:reto',
        [:name, :number]
      )
    ).not_to include(art)
  end

  it 'find within associated column' do
    cust = FactoryBot.create(:customer, name: 'holdener')
    art = FactoryBot.create(
      :article,
      customer: cust
    )
    expect(
      Article.like(
        'customer.name:holde',
        [{ customer: [:name] }]
      )
    ).to include(art)
  end

  it 'raise error if column is unknown' do
    FactoryBot.create(
      :article,
      name: 'hugo',
      number: 'alert'
    )
    expect {
      Article.like(
        'name3:hugo',
        [:name, :number]
      )
    }.to raise_error(RuntimeError, 'column «name3» is not present within available columns: «name, number»')
  end

  it 'raise error if column has Upper-Char' do
    FactoryBot.create(
      :article,
      name: 'hugo',
      number: 'alert'
    )
    expect {
      Article.like(
        'Name:hugo',
        [:name, :number]
      )
    }.to raise_error(RuntimeError, 'column «Name» is not present within available columns: «name, number»')
  end

  it 'raise error if associated column is unknown' do
    cust = FactoryBot.create(:customer, name: 'holdener')
    art = FactoryBot.create(
      :article,
      customer: cust
    )
    expect {
      Article.like(
        'customer.third_nam:holde',
        [{ customer: [:name] }]
      )
    }.to raise_error(RuntimeError, 'column «customer.third_nam» is not present within available columns: «customer.name»')
  end



end