require 'rails_helper'

RSpec.describe 'Translate Column', type: :model do

  it 'simple string: do nothing' do
    r = LikeQuery::Formatter.translate_columns('one two', [])
    expect(r).to eq('one two')
  end

  it 'simple translate' do
    r = LikeQuery::Formatter.translate_columns('one n:two', [['n','name']])
    expect(r).to eq('one name:two')
  end

  it 'match case-insensitive' do
    r = LikeQuery::Formatter.translate_columns('one N:two', [['n','name']], case_sensitive: false)
    expect(r).to eq('one name:two')
  end

  it 'case-sensitive should be default: do nothing' do
    r = LikeQuery::Formatter.translate_columns('one N:two', [['n','name']])
    expect(r).to eq('one N:two')
  end

  it 'symbols should work' do
    r = LikeQuery::Formatter.translate_columns('one n:two', [[:n,:name]])
    expect(r).to eq('one name:two')
  end

  it 'associated column' do
    r = LikeQuery::Formatter.translate_columns('one auth.n:marc', [['auth.n','author.name']])
    expect(r).to eq('one author.name:marc')
  end

end