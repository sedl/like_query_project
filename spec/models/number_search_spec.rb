require 'rails_helper'

RSpec.describe Article, type: :model do

  let(:customer) { Customer.create(name: 'Ambühl') }

  before(:each) do
    Customer.destroy_all
    Article.destroy_all
  end


  context 'find numbers' do

    context 'find within range' do

      context 'search-on-float-column' do
        it '.integer at lowest' do
          art = FactoryBot.create(:article, name: 'first', price: 3.0)
          expect(Article.like('3..4', :price)).to include(art)
        end
        it 'integer at highest' do
          art = Article.create(name: 'first', price: 4.0, customer: customer)
          expect(Article.like('3..4', :price)).to include(art)
        end
      end

      context 'search-on-integer-column' do
        context 'search-by-integer' do
          it '.integer at lowest' do
            art = FactoryBot.create(:article, name: 'first', stock_count: 3)
            expect(Article.like('3..5', :stock_count)).to include(art)
          end
          it 'integer at highest' do
            art = FactoryBot.create(:article, name: 'first', stock_count: 5)
            expect(Article.like('3..5', :stock_count)).to include(art)
          end
          it 'too high' do
            art = FactoryBot.create(:article, name: 'first', stock_count: 66)
            expect(Article.like('3..5', :stock_count)).not_to include(art)
          end
          it 'too low' do
            art = FactoryBot.create(:article, name: 'first', stock_count: 66)
            expect(Article.like('3..5', :stock_count)).not_to include(art)
          end
        end
        context 'search-by-float' do
          it 'too low' do
            art = FactoryBot.create(:article, name: 'first', stock_count: 3)
            expect(Article.like('4..5.0', :stock_count)).not_to include(art)
          end
        end
      end

    end

    context 'with associations' do
      it 'customer and article' do
        art = FactoryBot.create(:article, name: 'first', number: '01', customer: customer, price: 3.0)
        lq = Article.like('3..4', [:price, customer: :name])
        expect(lq).to include(art)
      end
    end

  end

  context 'find string and numbers' do

    before(:each) do
      @art = FactoryBot.create(:article, name: 'art-name', price: 4.6)
    end

    it 'find by name and price' do
      expect(Article.like('art-nam 3..5', [:name, :price])).to include(@art)
    end

    it 'when name is wrong: find-not' do
      expect(Article.like('art-nax 3..5', [:name, :price])).not_to include(@art)
    end

    it 'when number is wrong: find-not' do
      expect(Article.like('art-na 5..7', [:name, :price])).not_to include(@art)
    end
  end

  context 'find numbers within string' do

    it 'number with string' do
      art = FactoryBot.create(:article, name: 'first800next', price: 3.0)
      expect(Article.like('800', [:name, :price])).to include(art)
    end
  end

end