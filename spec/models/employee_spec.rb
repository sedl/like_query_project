require 'rails_helper'

RSpec.describe Employee, type: :model do
  it 'employable' do
    c = Customer.create(name: 'cust')
    e = Employee.create(name: 'hans', employable: c)
    s = Supplier.create(name: 'suppl')
    e2 = Employee.create(name: 'tobi', employable: s)
    expect(Employee.count).to eq(2)
  end
end
