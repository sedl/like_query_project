FactoryBot.define do
  factory :employee do
    name { "MyString" }
    employable_id { 1 }
    employable_type { "MyString" }
  end
end
