FactoryBot.define do
  factory :article do
    name { "MyString" }
    customer { FactoryBot.create(:customer) }
  end
end
