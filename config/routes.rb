Rails.application.routes.draw do
  resources :customer do
    member do
      resources :article, as: :customer_article
    end
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "customer#index"
end
